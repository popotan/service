package cn.rock.shiro.jwt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author rock
 * @date 2017/7/7
 */
@Component
public class JwtTokenProvider {
    private Logger logger = LoggerFactory.getLogger ( this.getClass () );
    private JwtConfig jwtConfig;

    public JwtTokenProvider(JwtConfig jwtConfig) {
        this.setConfiguration ( jwtConfig );
    }

    /**
     * 生成token
     *
     * @return
     */
    public String createToken(Claims claims) {
        ObjectMapper mapper = new ObjectMapper ();
        String compactJws = null;
        try {
            compactJws = Jwts.builder ().setPayload ( mapper.writeValueAsString ( claims ) ).compressWith ( CompressionCodecs.DEFLATE ).signWith ( SignatureAlgorithm.HS512, jwtConfig.getSecretKeySpec () ).compact ();
        } catch (JsonProcessingException e) {
            e.printStackTrace ();
        }
        return compactJws;
    }

    /**
     * token转换
     */
    public Claims parseToken(String token) {
        logger.info ( "token信息： " + token );
        Claims claims = null;
        try {
            claims = Jwts.parser ().setSigningKey ( jwtConfig.getSecretKeySpec () ).parseClaimsJws ( token ).getBody ();
        } catch (Exception e) {
            e.printStackTrace ();
        }
        logger.info ( "claims信息： " + claims );
        return claims;
    }


    public JwtConfig getConfiguration() {
        return jwtConfig;
    }

    public void setConfiguration(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

}
