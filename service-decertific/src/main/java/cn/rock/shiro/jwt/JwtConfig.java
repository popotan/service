package cn.rock.shiro.jwt;

import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;

/**
 * @author rock
 * @date 2017/7/7
 */
@Component
@ConfigurationProperties("shiro.token.jwt")
public class JwtConfig {
    private String key;
    private String iss;
    /** 有效期：分钟 */
    private int expm;
    /** APP-有效期：分钟 */
    private int expmapp;
    /** 剩余有效期-刷新token用：分钟 */
    private int refreshExp;
    private int refreshExpApp;

    public int getExpm() {
        return expm;
    }
    public void setExpm(int expm) {
        this.expm = expm;
    }

    public String getIss() {
        return iss;
    }

    public void setIss(String iss) {
        this.iss = iss;
    }

    private String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getExpmapp() {
        return expmapp;
    }

    public void setExpmapp(int expmapp) {
        this.expmapp = expmapp;
    }

    public int getRefreshExp() {
        return refreshExp;
    }

    public void setRefreshExp(int refreshExp) {
        this.refreshExp = refreshExp;
    }

    public int getRefreshExpApp() {
        return refreshExpApp;
    }

    public void setRefreshExpApp(int refreshExpApp) {
        this.refreshExpApp = refreshExpApp;
    }

    public SecretKeySpec getSecretKeySpec() {
        return new SecretKeySpec(this.getKey()
                .getBytes(), SignatureAlgorithm.HS512.getJcaName());
    }

}
