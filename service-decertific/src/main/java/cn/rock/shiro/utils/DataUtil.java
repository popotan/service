package cn.rock.shiro.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author rock
 * @date 2017/7/7
 */
public class DataUtil {
    /**
     * 根据指定的格式,格式化日期
     * @param date java8 之前的传统日期
     * @param formatter 格式化
     * @return 格式化字符串
     */
    public static String format(Date date, Formatter formatter) {
        return format(asLocalDateTime(date), formatter);
    }
    public static LocalDateTime asLocalDateTime(Date date) {
        return date == null ? null:Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
    /**
     * 根据指定的格式,格式化日期
     * @param date java8中的日期
     * @param formatter 格式化
     * @return 格式化字符串
     */
    public static String format(LocalDateTime date,Formatter formatter){
        return formatter.format(date);
    }
    /**
     * 使用参数Format将字符串转为Date
     */
    public static Date parse(String strDate, String pattern)
            throws ParseException {
        return isEmpty(strDate) ? null : new SimpleDateFormat (
                pattern).parse(strDate);
    }
    public static boolean isEmpty(String str) {
        return (str == null || str.trim().length() == 0);
    }

    /**
     * 获取当前时间
     * @return
     */
    public static Date now(){
        return new Date();
    }

    /**
     * 日期格式化器
     */
    public enum Formatter{

        MM_dd("MM-dd"),
        yyyyMMdd("yyyyMMdd"),
        HHmmss("HHmmss"),
        yyyy_MM_dd("yyyy-MM-dd"),
        yyyyMMddHHmm("yyyy-MM-dd HH:mm"),
        yyyyMMddHHmmss("yyyy-MM-dd HH:mm:ss"),
        yyyyMMddHHmmssTrim("yyyyMMddHHmmss"),
        ddHHmmssTrim("ddHHmmss"),
        yyyy_MM_dd_zh("yyyy年MM月dd日"),
        yyyyMMddHHmmss_zh("yyyy年MM月dd日 HH:mm:ss"),
        yyyyMMddHHmm_zh("yyyy年MM月dd日 HH:mm"),
        ;

        /** 线程安全,可共享 */
        private DateTimeFormatter formatter;

        Formatter(String pattern) {
            this.formatter = DateTimeFormatter.ofPattern(pattern);
        }

        public String format(LocalDateTime localDateTime) {
            return formatter.format(localDateTime);
        }

        public String format(LocalDate localDate) {
            return formatter.format(localDate);
        }

    }
}
