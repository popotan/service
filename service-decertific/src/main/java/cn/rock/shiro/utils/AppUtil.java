package cn.rock.shiro.utils;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author rock
 * @date 2017/7/7
 */
public class AppUtil {
    public static Boolean isApp() {
        String ua = getHeader("User-Agent-from");
        if(DataUtil.isEmpty(ua)) {
            ua = getHeader("User-Agent");
        }

        boolean flag = false;
        if(!DataUtil.isEmpty(ua) && ua.startsWith("mobile")) {
            flag = true;
        }
        return Boolean.valueOf(flag);
    }
    public static String getAuthorization() {
        return getHeader("Authorization");
    }
    private static String getHeader(String key) {
        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        return sra.getRequest().getHeader(key);
    }
}
