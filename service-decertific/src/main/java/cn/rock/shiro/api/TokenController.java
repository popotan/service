package cn.rock.shiro.api;

import cn.rock.dto.ResponseMessage;
import cn.rock.shiro.jwt.JwtConfig;
import cn.rock.shiro.jwt.JwtTokenProvider;
import cn.rock.shiro.jwt.ShiroClaims;
import cn.rock.shiro.po.TokenPojo;
import cn.rock.shiro.utils.AppUtil;
import cn.rock.shiro.utils.DataUtil;
import io.jsonwebtoken.Claims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
/**
 * @author rock
 * @date 2017/7/7
 */
@RestController
public class TokenController {
    private Logger logger = LoggerFactory.getLogger ( this.getClass () );

    @Autowired
    JwtConfig jwtConfig;
    @Autowired
    JwtTokenProvider jwtTokenProvider;
    @Resource
    RedisTemplate<String, Integer> redisTemplate;
    @Autowired
    StringRedisTemplate stringRedisTemplate;



    /**
     * token 转换
     */
    @RequestMapping("/token/parse")
    public Claims parseToken(String token) {
        return jwtTokenProvider.parseToken ( token );
    }

    @RequestMapping("/token/getTokenPhone")
    public String getTokenPhone(String token) {
        Claims claims = jwtTokenProvider.parseToken ( token );
        if (claims == null) return null;//有可能token已过期
        return claims.get ( "phone", String.class );
    }
    /**
     * 刷新jwt
     */
    @RequestMapping("/token/refresh")
    public ResponseEntity<?> refreshToken() {
        String token = AppUtil.getAuthorization();
        Claims claims = jwtTokenProvider.parseToken(token);
        Date date = claims.getExpiration();
        int plus = (int) (date.getTime() - System.currentTimeMillis());
        int refreshExp = AppUtil.isApp() ? jwtConfig.getRefreshExpApp() : jwtConfig.getRefreshExp();
        if(plus > 0 && plus < refreshExp * 1000 * 60){//有效期时间内有动作的话给续期
            //作废之前的token
            BoundValueOperations<String,Integer> boundValueOperations = redisTemplate.boundValueOps(token);
            boundValueOperations.set(1);
            boundValueOperations.expireAt(date);
            //设置新的有效期，生成新的token
            return ResponseEntity.ok(rebuildJWTToken(claims));
        }
        else {
            return ResponseEntity.ok(new TokenPojo (token, plus, Long.parseLong(claims.getAudience())));
        }
    }


    /**
     * 获取新的token
     */
    @RequestMapping(method = RequestMethod.POST, path = "/token/getNewToken")
    public String getNewToken(String token) {
        if (DataUtil.isEmpty ( token )) {
            return null;
        }
        int refreshExp = jwtConfig.getRefreshExpApp ();
        Claims claims = jwtTokenProvider.parseToken ( token );
        Date date = claims.getExpiration ();
        int plus = (int) (date.getTime () - System.currentTimeMillis ());
        if (plus > 0 && plus < refreshExp * 1000 * 60) {//还剩10分钟有效期时间内有动作的话给续期
            //作废之前的token
            BoundValueOperations<String, Integer> boundValueOperations = redisTemplate.boundValueOps ( token );
            boundValueOperations.set ( 1 );
            boundValueOperations.expireAt ( date );
            //设置新的有效期，生成新的token
            return rebuildJWTToken ( claims ).getAccessToken ();
        }
        return null;
    }

    /**
     * 校验jwt有效性
     */
    @RequestMapping("/token/verify")
    public Boolean verifyToken(String token) {
        Claims claims = jwtTokenProvider.parseToken ( token );
        //去检查用户有没有修改过密码
        BoundValueOperations<String, String> ops = stringRedisTemplate.boundValueOps ( "modify_password_" + claims.getAudience () );
        if (ops.get () != null) {
            try {
                if (claims.getIssuedAt ().before ( DataUtil.parse ( ops.get (), "yyyy-MM-dd HH:mm:ss" ) )) {//token在修改密码之前的
                    return false;
                }
            } catch (ParseException e) {
                e.printStackTrace ();
                return false;
            }
        }
        //检查token是否有效-redis里有记录表明已主动退出或更换token，token已失效
        BoundValueOperations<String, Integer> boundValueOperations = redisTemplate.boundValueOps ( token );
        Boolean result = (boundValueOperations.get () == null);
        return result;
    }


    private TokenPojo rebuildJWTToken(Claims claims) {
        long ex = jwtConfig.getExpm() * 1000 * 60;
        if (AppUtil.isApp()){
            ex = jwtConfig.getExpmapp() * 1000 * 60;
        }
        Date now = new Date();
        claims.setIssuedAt(now);
        claims.setNotBefore(now);
        claims.setExpiration(new Date(System.currentTimeMillis() + ex));
        claims.setId(UUID.randomUUID().toString());
        return new TokenPojo (jwtTokenProvider.createToken(claims), ex, Long.parseLong(claims.getAudience()));
    }

    private ShiroClaims buildClaims(String userId, String username, String email, String phone, long ex) {
        Date now = new Date ();
        ShiroClaims uaaClaims = new ShiroClaims ();
        uaaClaims.setIssuer ( jwtConfig.getIss () );
        uaaClaims.setIssuedAt ( now );
        uaaClaims.setAudience ( userId );
        uaaClaims.setId ( UUID.randomUUID ().toString () );
        uaaClaims.setUserName ( username );
        uaaClaims.setExpiration ( new Date ( System.currentTimeMillis () + ex ) );
        uaaClaims.setEmail ( email );
        uaaClaims.setPhone ( phone );
        uaaClaims.setSubject ( userId );
        uaaClaims.setNotBefore ( now );
        return uaaClaims;

    }
    /**
     * 登出
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, path = "/logout")
    public ResponseMessage logout() {
        String token = AppUtil.getAuthorization();
        if(DataUtil.isEmpty(token)) return ResponseMessage.success(false);
        Claims claims = jwtTokenProvider.parseToken(token);
        BoundValueOperations boundValueOperations = redisTemplate.boundValueOps(token);
        boundValueOperations.set(1);
        boundValueOperations.expireAt(claims.getExpiration());
        return ResponseMessage.success(true);
    }

    /**
     * 踢掉在线用户
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, path = "/kickOnlieUser")
    public ResponseMessage kickOnlieUser(Long userId) {
        BoundValueOperations boundValueOperations = stringRedisTemplate.boundValueOps("modify_password_" + userId);
        boundValueOperations.set(DataUtil.format(DataUtil.now(), DataUtil.Formatter.yyyyMMddHHmmss));
        if (AppUtil.isApp()){
            boundValueOperations.expire(jwtConfig.getExpmapp(),TimeUnit.MINUTES);
        }else {
            boundValueOperations.expire( jwtConfig.getExpm(), TimeUnit.MINUTES);
        }
        return ResponseMessage.success(true);
    }


}
