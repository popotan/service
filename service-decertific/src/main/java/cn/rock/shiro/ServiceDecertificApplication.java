package cn.rock.shiro;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * 鉴权服务
 */

@SpringCloudApplication
@EnableFeignClients("cn.rock.api")
@ComponentScan(basePackages = {"cn.rock.shiro" , "cn.rock"})
public class ServiceDecertificApplication {

    public static void main(String[] args) {
        SpringApplication.run ( ServiceDecertificApplication.class, args );
    }
}
