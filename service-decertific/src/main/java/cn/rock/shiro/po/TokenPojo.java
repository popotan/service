package cn.rock.shiro.po;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author rock
 * @date 2017/7/7
 */
public class TokenPojo {
    private String accessToken;
    private long expiresIn;
    private long userId;

    public String getAccessToken() {
        return accessToken;
    }

    @JsonProperty("access_token")
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    @JsonProperty("expires_in")
    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public TokenPojo(String accessToken, long expiresIn, long userId) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.userId = userId;
    }
}
