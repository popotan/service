package cn.rock.shiro.po;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author rock
 * @date 2017/7/7
 */
public class LoginPojo {
    @Size(min = 1, max = 11)
    private String phone;

    @NotNull
    private String password;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
