package cn.rock.demo.api;

import cn.rock.api.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author rock
 * @date 2017/7/7
 */
@RestController
public class DemoServiceImpl {
    @Autowired
    private NewsService newsService;

    @GetMapping("/rock")
    public String rock(@RequestParam("rock") String rock) {

        String wow = newsService.cnm ( "news-service 返回数据" );
        return rock + wow;
    }

    @GetMapping("/cnm")
    public String cnm(@RequestParam("cnm") String cnm) {
        return "这是rock 服务" + cnm;
    }
}
