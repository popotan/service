package cn.rock.demo;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@SpringCloudApplication
@EnableFeignClients("cn.rock.api")
public class ServiceHiApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder ( ServiceHiApplication.class ).web ( true ).run ( args );
    }
}
