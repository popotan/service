package cn.rock;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * 一个 cloud service
 * */
@SpringCloudApplication
@EnableFeignClients
public class ServiceNewsApplication {

	public static void main(String[] args) {
		SpringApplication.run( ServiceNewsApplication.class, args);
	}
}
