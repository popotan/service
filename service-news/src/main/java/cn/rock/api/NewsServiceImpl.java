package cn.rock.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author rock
 * @date 2017/7/7
 */
@RestController
public class NewsServiceImpl {

    @Autowired
    private RockService rockService;

    @GetMapping("/wow")
    public String wow(@RequestParam("wow") String wow) {
        String rock = rockService.cnm ( "rock-service 返回数据" );
        return wow + rock;
    }

    @GetMapping("/cnm")
    public String cnm(@RequestParam("cnm") String cnm) {
        return "这是news 服务" + cnm;
    }
}
